package com.dratwscode.poapp.login


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import com.dratwscode.poapp.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class AddTripTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(StartPageActivity::class.java)

    @Test
    fun addTripTest() {
        val appCompatButton = onView(
            allOf(
                withId(R.id.startPage_tourist), withText("TURYSTA"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        val appCompatImageButton = onView(
            allOf(
                childAtPosition(
                    allOf(
                        withId(R.id.t_main_toolbar),
                        childAtPosition(
                            withClassName(`is`("android.widget.LinearLayout")),
                            0
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatImageButton.perform(click())

        val navigationMenuItemView = onView(
            allOf(
                childAtPosition(
                    allOf(
                        withId(R.id.design_navigation_view),
                        childAtPosition(
                            withId(R.id.t_main_navigationView),
                            0
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        navigationMenuItemView.perform(click())

        val tabView = onView(
            allOf(
                withContentDescription("AKTYWNE"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_books_tab_layout),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        tabView.perform(click())

        val appCompatImageView = onView(
            allOf(
                withId(R.id.t_books_list_image), withContentDescription("zdjecie odznaki"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_books_list_recycler_view),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatImageView.perform(click())

        val floatingActionButton = onView(
            allOf(
                withId(R.id.t_trips_fab),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_main_nav_host),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        floatingActionButton.perform(click())

        val appCompatTextView = onView(
            allOf(
                withId(R.id.t_add_trip_data_item_name), withText("Rusinowa Polana"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_data_recycler),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatTextView.perform(click())

        val textView = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Rusinowa Polana"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Rusinowa Polana")))

        val textView2 = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Rusinowa Polana"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView2.check(matches(isDisplayed()))

        val appCompatTextView2 = onView(
            allOf(
                withId(R.id.t_add_trip_data_item_name), withText("Polana pod Wołoszynem"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_data_recycler),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatTextView2.perform(click())

        val textView3 = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Polana pod Wołoszynem"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        1
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView3.check(matches(isDisplayed()))

        val textView4 = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Polana pod Wołoszynem"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        1
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView4.check(matches(withText("Polana pod Wołoszynem")))

        val appCompatTextView3 = onView(
            allOf(
                withId(R.id.t_add_trip_data_item_name), withText("Rówień Waksmundzka"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_data_recycler),
                        1
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        appCompatTextView3.perform(click())

        val textView5 = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Rówień Waksmundzka"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView5.check(matches(isDisplayed()))

        val textView6 = onView(
            allOf(
                withId(R.id.t_add_trip_result_item_name), withText("Rówień Waksmundzka"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.t_add_trip_result_recycler),
                        2
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView6.check(matches(withText("Rówień Waksmundzka")))

        val textView7 = onView(
            allOf(
                withId(R.id.t_add_trip_points), withText("5"),
                childAtPosition(
                    childAtPosition(
                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                        0
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        textView7.check(matches(isDisplayed()))

        val textView8 = onView(
            allOf(
                withId(R.id.t_add_trip_points), withText("5"),
                childAtPosition(
                    childAtPosition(
                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                        0
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        textView8.check(matches(withText("5")))

        val appCompatButton3 = onView(
            allOf(
                withId(R.id.t_add_trip_confirm), withText("POTWIERDZ"),
                childAtPosition(
                    childAtPosition(
                        withClassName(`is`("android.widget.FrameLayout")),
                        0
                    ),
                    6
                ),
                isDisplayed()
            )
        )
        appCompatButton3.perform(click())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
