package com.dratwscode.poapp.login


import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.Root
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.dratwscode.poapp.R
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.Espresso.onView
import com.dratwscode.poapp.admin.views.SegmentAddActivity
import com.google.android.material.internal.ContextUtils.getActivity
import org.junit.Before


@LargeTest
@RunWith(AndroidJUnit4::class)
class AddSegmentTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(StartPageActivity::class.java)



    @Test
    fun addSegmentTest() {
        val appCompatButton = onView(
            allOf(
                withId(R.id.startPage_admin), withText("ADMINISTRATOR"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        val floatingActionButton = onView(
            allOf(
                withId(R.id.a_segments_add_button),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.a_main_nav_host),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        floatingActionButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        val appCompatAutoCompleteTextView = onView(
            allOf(
                withId(R.id.a_start_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView.perform(click())

        val appCompatAutoCompleteTextView2 = onView(
            allOf(
                withId(R.id.a_start_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView2.perform(replaceText("L"), closeSoftKeyboard())

        onView(withText("Leszna Górna, Beskid Śląski"))
            .inRoot(withDecorView(not(`is`(mActivityTestRule.activity!!.getWindow().getDecorView()))))
            .perform(click())

        val editText = onView(
            allOf(
                withId(R.id.a_start_point_text_view), withText("Leszna Górna, Beskid Śląski"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        editText.check(matches(withText("Leszna Górna, Beskid Śląski")))

        val appCompatAutoCompleteTextView3 = onView(
            allOf(
                withId(R.id.a_end_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    4
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView3.perform(click())

        appCompatAutoCompleteTextView3.perform(replaceText("S"), closeSoftKeyboard())

        onView(withText("Schronisko PTTK Tuł, Beskid Śląski"))
            .inRoot(withDecorView(not(`is`(mActivityTestRule.activity!!.getWindow().getDecorView()))))
            .perform(click())

        val editText2 = onView(
            allOf(
                withId(R.id.a_end_point_text_view), withText("Schronisko PTTK Tuł, Beskid Śląski"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    4
                ),
                isDisplayed()
            )
        )
        editText2.check(matches(withText("Schronisko PTTK Tuł, Beskid Śląski")))

//        pressBack()

        val editText3 = onView(
            allOf(
                withId(R.id.a_first_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    8
                ),
                isDisplayed()
            )
        )
        editText3.check(matches(isDisplayed()))

        val editText4 = onView(
            allOf(
                withId(R.id.a_second_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    11
                ),
                isDisplayed()
            )
        )
        editText4.check(matches(isDisplayed()))

        val textView = onView(
            allOf(
                withId(R.id.a_first_weight_label), withText("Leszna Górna - Schronisko PTTK Tuł"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    7
                ),
                isDisplayed()
            )
        )
        textView.check(matches(isDisplayed()))

        val textView2 = onView(
            allOf(
                withId(R.id.a_second_weight_label), withText("Schronisko PTTK Tuł - Leszna Górna"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    10
                ),
                isDisplayed()
            )
        )
        textView2.check(matches(isDisplayed()))

        val textView3 = onView(
            allOf(
                withId(R.id.a_first_weight_label), withText("Leszna Górna - Schronisko PTTK Tuł"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    7
                ),
                isDisplayed()
            )
        )
        textView3.check(matches(withText("Leszna Górna - Schronisko PTTK Tuł")))

        val textView4 = onView(
            allOf(
                withId(R.id.a_second_weight_label), withText("Schronisko PTTK Tuł - Leszna Górna"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    10
                ),
                isDisplayed()
            )
        )
        textView4.check(matches(withText("Schronisko PTTK Tuł - Leszna Górna")))

        val appCompatEditText = onView(
            allOf(
                withId(R.id.a_first_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    8
                ),
                isDisplayed()
            )
        )
        appCompatEditText.perform(click())
        appCompatEditText.perform(replaceText("5"), closeSoftKeyboard())

//        pressBack()

        val appCompatEditText2 = onView(
            allOf(
                withId(R.id.a_second_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    11
                ),
                isDisplayed()
            )
        )
        appCompatEditText2.perform(click())
        appCompatEditText2.perform(replaceText("4"), closeSoftKeyboard())

//        pressBack()

        Thread.sleep(100)
        val appCompatButton2 = onView(
            allOf(
                withId(R.id.submit_button), withText("Zakończ"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    13
                ),
                isDisplayed()
            )
        )
        appCompatButton2.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        pressBack()
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
