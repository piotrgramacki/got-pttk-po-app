package com.dratwscode.poapp.login


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.dratwscode.poapp.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class DoNotAddSegmentTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(StartPageActivity::class.java)

    @Test
    fun doNotAddSegmentTest() {
        val appCompatButton = onView(
            allOf(
                withId(R.id.startPage_admin), withText("ADMINISTRATOR"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        val floatingActionButton = onView(
            allOf(
                withId(R.id.a_segments_add_button),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.a_main_nav_host),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        floatingActionButton.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(700)

        val appCompatAutoCompleteTextView = onView(
            allOf(
                withId(R.id.a_start_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView.perform(click())

        val appCompatAutoCompleteTextView2 = onView(
            allOf(
                withId(R.id.a_start_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView2.perform(typeText("Wrong segment"), closeSoftKeyboard())

        val textView = onView(
            allOf(
                withId(R.id.enter_start_correct_error_text), withText("Wybierz poprawny punkt"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        textView.check(matches(isDisplayed()))

        val appCompatAutoCompleteTextView3 = onView(
            allOf(
                withId(R.id.a_start_point_text_view), withText("Wrong segment"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView3.perform(click())

        val appCompatAutoCompleteTextView4 = onView(
            allOf(
                withId(R.id.a_start_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView4.perform(clearText(), typeText("J"))

        val appCompatAutoCompleteTextView5 = onView(
            allOf(
                withId(R.id.a_start_point_text_view), withText("J"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView5.perform(closeSoftKeyboard())

        onView(withText("Jasieniowa, Beskid Śląski"))
            .inRoot(RootMatchers.withDecorView(not(`is`(mActivityTestRule.activity!!.getWindow().getDecorView()))))
            .perform(click())

        val appCompatAutoCompleteTextView6 = onView(
            allOf(
                withId(R.id.a_end_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    4
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView6.perform(click())
        appCompatAutoCompleteTextView6.perform(typeText("Leszny"), closeSoftKeyboard())

        val textView2 = onView(
            allOf(
                withId(R.id.enter_end_correct_error_text), withText("Wybierz poprawny punkt"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        textView2.check(matches(withText("Wybierz poprawny punkt")))

        val appCompatAutoCompleteTextView7 = onView(
            allOf(
                withId(R.id.a_end_point_text_view),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    4
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView7.perform(click())
        appCompatAutoCompleteTextView7.perform(clearText())
        appCompatAutoCompleteTextView7.perform(typeText("Leszn"))


        val appCompatAutoCompleteTextView8 = onView(
            allOf(
                withId(R.id.a_end_point_text_view), withText("Leszn"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    4
                ),
                isDisplayed()
            )
        )
        appCompatAutoCompleteTextView8.perform(closeSoftKeyboard())

        onView(withText("Leszna Górna, Beskid Śląski"))
            .inRoot(RootMatchers.withDecorView(not(`is`(mActivityTestRule.activity!!.getWindow().getDecorView()))))
            .perform(click())

        val textView3 = onView(
            allOf(
                withId(R.id.a_first_weight_label), withText("Jasieniowa - Leszna Górna"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    7
                ),
                isDisplayed()
            )
        )
        textView3.check(matches(withText("Jasieniowa - Leszna Górna")))

        val textView4 = onView(
            allOf(
                withId(R.id.a_second_weight_label), withText("Leszna Górna - Jasieniowa"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    10
                ),
                isDisplayed()
            )
        )
        textView4.check(matches(withText("Leszna Górna - Jasieniowa")))

        val appCompatEditText10 = onView(
            allOf(
                withId(R.id.a_first_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    8
                ),
                isDisplayed()
            )
        )
        appCompatEditText10.perform(typeText("10"), closeSoftKeyboard())


        val appCompatEditText11 = onView(
            allOf(
                withId(R.id.a_second_weight),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    11
                ),
                isDisplayed()
            )
        )
        appCompatEditText11.perform(typeText("5"), closeSoftKeyboard())

        val button = onView(
            allOf(
                withId(R.id.submit_button),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    13
                ),
                isDisplayed()
            )
        )
        button.check(matches(isDisplayed()))

        val appCompatButton2 = onView(
            allOf(
                withId(R.id.cancel_button), withText("Anuluj"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    14
                ),
                isDisplayed()
            )
        )
        appCompatButton2.perform(click())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
