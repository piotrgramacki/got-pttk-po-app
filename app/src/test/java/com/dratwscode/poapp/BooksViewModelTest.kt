package com.dratwscode.poapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dratwscode.poapp.communication.entities.KsiazeczkaTO
import com.dratwscode.poapp.communication.entities.NazwaStopniaTO
import com.dratwscode.poapp.communication.entities.StopienTO
import com.dratwscode.poapp.communication.entities.UzytkownikTO
import com.dratwscode.poapp.communication.requesters.impl.tourist.BookTouristRequester
import com.dratwscode.poapp.tourist.viewmodels.BooksViewModel
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.*
import java.sql.Date


class BooksViewModelTest {
    private val requester = mock(BookTouristRequester::class.java)
    private val booksList = mutableListOf<KsiazeczkaTO>()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    fun addClosed() {
        booksList.add(KsiazeczkaTO(
            id = 0,
            stopien = StopienTO(id = 1, nazwa_stopnia = NazwaStopniaTO(1, "W góry brązowa"), wymagana_liczba_punktow = 15),
            uzytkownik = UzytkownikTO(3, "", "", "", "", Date(System.currentTimeMillis()), Date(System.currentTimeMillis()),
                czy_administrator = false,
                czy_turysta = true
            ),
            stan_punktow = 20,
            czy_aktywna = false
        )
        )
    }

    fun addLast() {
        booksList.add(KsiazeczkaTO(
            id = 0,
            stopien = StopienTO(id = 7, nazwa_stopnia = NazwaStopniaTO(7, "Mala zlota"), wymagana_liczba_punktow = 720),
            uzytkownik = UzytkownikTO(3, "", "", "", "", Date(System.currentTimeMillis()), Date(System.currentTimeMillis()),
                czy_administrator = false,
                czy_turysta = true
            ),
            stan_punktow = 732,
            czy_aktywna = false
        )
        )
    }

    fun addActive() {
        booksList.add(KsiazeczkaTO(
            id = 0,
            stopien = StopienTO(id = 2, nazwa_stopnia = NazwaStopniaTO(2, "W góry srebrna"), wymagana_liczba_punktow = 30),
            uzytkownik = UzytkownikTO(3, "", "", "", "", Date(System.currentTimeMillis()), Date(System.currentTimeMillis()),
                czy_administrator = false,
                czy_turysta = true
            ),
            stan_punktow = 28,
            czy_aktywna = true
        )
        )
    }

    @Test
    fun canAdd_for_no_active_book_should_return_true() {
        addClosed()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.canAdd()

        assertTrue(result)
    }

    @Test
    fun canAdd_for_active_book_should_return_false() {
        addClosed()
        addActive()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.canAdd()

        assertFalse(result)
    }

    @Test
    fun getLastBadgeId_for_one_book_should_return_its_id() {
        addClosed()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getLastBadgeId()

        assertEquals(1, result)
    }

    @Test
    fun getLastBadgeId_for_multiple_books_should_return_last_id() {
        addClosed()
        addLast()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getLastBadgeId()

        assertEquals(7, result)
    }

    @Test
    fun getLastBadgeId_for_empty_booksList_should_return_zero() {
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getLastBadgeId()

        assertEquals(0, result)
    }

    @Test
    fun getOverflow_for_empty_booksList_should_return_zero() {
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getOverflow()

        assertEquals(0, result)
    }

    @Test
    fun getOverflow_for_one_book_with_less_than_needed_should_return_zero() {
        addActive()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getOverflow()

        assertEquals(0, result)
    }

    @Test
    fun getOverflow_for_multiple_books_with_last_less_than_needed_should_return_zero() {
        addActive()
        addClosed()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getOverflow()

        assertEquals(0, result)
    }

    @Test
    fun getOverflow_for_book_with_overflow_should_return_correct_overflow() {
        addClosed()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getOverflow()

        assertEquals(5, result)
    }

    @Test
    fun getOverflow_for_multiple_books_should_return_correct_overflow_from_last() {
        addClosed()
        addLast()
        `when`(requester.getAll).thenReturn(booksList)
        val viewModel = BooksViewModel(requester)

        val result = viewModel.getOverflow()

        assertEquals(12, result)
    }
}