package com.dratwscode.poapp.admin.model

import java.sql.Date

class Spot(val id: Int, val name: String, val subGroup: SubGroup, val startDate: Date, val endDate: Date?) {
    val subGroupString = subGroup.name

    val group = subGroup.group

    val groupString = subGroup.group.name
}