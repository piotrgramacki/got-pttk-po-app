package com.dratwscode.poapp.admin.views

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.dratwscode.poapp.R
import com.dratwscode.poapp.admin.model.Spot

import java.util.ArrayList

class SpotsAdapter(context: Context, private val viewResourceId: Int, private val items: ArrayList<Spot>) :
    ArrayAdapter<Spot>(context, viewResourceId, items) {
    private var itemsAll: ArrayList<Spot> = items.clone() as ArrayList<Spot>
    private val suggestions: ArrayList<Spot> = ArrayList()
    private var resetted: Boolean = true

    internal var nameFilter: Filter = object : Filter() {
        override fun convertResultToString(resultValue: Any): String? {
            return (resultValue as Spot).name + ", " + resultValue.subGroup.name
        }

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            if (constraint != null) {
                suggestions.clear()
                for (spot in itemsAll) {
                    if (spot.name.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(spot)
                    }
                }
                val filterResults = Filter.FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions.size
                return filterResults
            } else {
                return Filter.FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            if (results.count > 0) {
                val filteredList = results.values as ArrayList<Spot>
                clear()
                for (c in filteredList) {
                    add(c)
                }
                notifyDataSetChanged()
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var v = convertView
        if (v == null) {
            val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            v = vi.inflate(viewResourceId, null)
        }
        val spot = items[position]
        val spotLabel = v as TextView
        val textValue = spot.name + ", " + spot.subGroup.name
        spotLabel.text = textValue

        return v
    }

    override fun getFilter(): Filter {
        return nameFilter
    }

    fun filterSpots(chosenSpot: Spot) {
        val filter = itemsAll.filter { spot -> spot.subGroup.name == chosenSpot.subGroup.name }
        itemsAll = filter as ArrayList<Spot>
        resetted = false
    }

    fun resetSpots(dataset: List<Spot>) {
        itemsAll = dataset as ArrayList<Spot>
        resetted = true
    }

    fun getResetd(): Boolean {
        return resetted
    }


}