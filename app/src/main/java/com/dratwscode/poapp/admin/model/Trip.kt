package com.dratwscode.poapp.admin.model

import java.sql.Date

class Trip(val id: Int, val book: Book, val date: Date, val points: Int)