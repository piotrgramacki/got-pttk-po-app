package com.dratwscode.poapp.admin.model

class SubGroup(val id : Int, val name: String, val code: String, val group: Group)