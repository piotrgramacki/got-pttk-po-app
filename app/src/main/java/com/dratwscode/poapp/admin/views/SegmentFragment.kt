package com.dratwscode.poapp.admin.views

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.dratwscode.poapp.R
import com.dratwscode.poapp.admin.viewmodels.SegmentsViewModel
import kotlinx.android.synthetic.main.admin_fragment_segment.*

class SegmentFragment : Fragment() {

    companion object {
        fun newInstance() = SegmentFragment()
    }

    private lateinit var viewModel: SegmentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.admin_fragment_segment, container, false)
    }

    private fun initializeRecyclerView() {
        a_segments_recycler_view.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(activity)

            adapter = SegmentsAdapter(context, viewModel.getDataset())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SegmentsViewModel::class.java)

        initializeRecyclerView()
        initializeAddButton()
    }

    private fun initializeAddButton() {
        a_segments_add_button.setOnClickListener {
            val intent = Intent(context, com.dratwscode.poapp.admin.views.SegmentAddActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        a_segments_recycler_view.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(activity)

            adapter = SegmentsAdapter(context, viewModel.getDataset())
        }
    }


}
