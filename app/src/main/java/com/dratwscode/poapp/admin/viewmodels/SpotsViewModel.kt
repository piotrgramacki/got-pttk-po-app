package com.dratwscode.poapp.admin.viewmodels

import androidx.lifecycle.ViewModel
import com.dratwscode.poapp.communication.requesters.impl.admin.SpotsAdminRequester
import com.dratwscode.poapp.admin.model.Spot
import com.dratwscode.poapp.admin.viewmodels.mappers.Mapper

class SpotsViewModel : ViewModel() {

    val requester = SpotsAdminRequester.getInstance()

    fun getDataset(): List<Spot> {
        val listOfSpots = requester.getAll
        return listOfSpots.map { punktTO -> Mapper.spotFromTO(punktTO) }.toList()
    }
}