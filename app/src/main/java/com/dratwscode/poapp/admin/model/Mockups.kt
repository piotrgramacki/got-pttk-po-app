package com.dratwscode.poapp.admin.model

import com.dratwscode.poapp.communication.entities.NazwaStopniaTO
import com.dratwscode.poapp.R
import com.dratwscode.poapp.communication.entities.StopienTO
import java.sql.Date

object Mockups {
    private var initializedSegments = false
    private var initializedBadges = false
    private val segments: MutableList<Segment> = mutableListOf()
    private val badges: MutableList<Pair<StopienTO, Int>> = mutableListOf()

    val segmentsData: MutableList<Segment>
        get() {
            if (!initializedSegments)
                initializeSegments()
            return segments
        }

    val badgesData: MutableList<Pair<StopienTO, Int>>
        get() {
            if (!initializedBadges)
                initializeBadges()
            return badges
        }

    private fun initializeBadges() {
        if (!initializedBadges) {
            badges.add(Pair(StopienTO(1, NazwaStopniaTO(1, "\"W góry\" brązowa"), 15), R.drawable.w_gory_brazowa))
            badges.add(Pair(StopienTO(2, NazwaStopniaTO(2, "\"W góry\" srebrna"), 40), R.drawable.w_gory_srebrna))
            badges.add(Pair(StopienTO(3, NazwaStopniaTO(3, "\"W góry\" złota"), 40), R.drawable.w_gory_zlota))
            badges.add(Pair(StopienTO(4, NazwaStopniaTO(4, "Popularna"), 60), R.drawable.popularna_image_view))
            badges.add(Pair(StopienTO(5, NazwaStopniaTO(5, "Mała brązowa"), 120), R.drawable.mala_brazowa_image_view))
            badges.add(Pair(StopienTO(6, NazwaStopniaTO(6, "Mała srebrna"), 360), R.drawable.mala_srebrna_image_view))
            badges.add(Pair(StopienTO(7, NazwaStopniaTO(7, "Mała złota"), 720), R.drawable.mala_zlota_image_view))

            initializedBadges = true
        }
    }

    private fun initializeSegments() {
        if (!initializedSegments) {
            // groups
            val sudety = Group(1, "Sudety")

            //subgroups
            val karkonosze = SubGroup(1, "Karkonosze", "S.03", sudety)
            val izery = SubGroup(1, "Gory Izerskie", "S.01", sudety)

            //points
            val szrenica = Spot(0, "Szrenica", karkonosze, Date(System.currentTimeMillis()), null)
            val hala_szrenica = Spot(0, "Hala Szrenicka", karkonosze, Date(System.currentTimeMillis()), null)
            val mokra_prz = Spot(0, "Mokra Przelecz", karkonosze, Date(System.currentTimeMillis()), null)
            val labski =
                Spot(0, "Schr. PTTK Hala pod Labskim Szczytem", karkonosze, Date(System.currentTimeMillis()), null)

            //segments
            segments.add(Segment(1, szrenica, hala_szrenica, 1, Date(System.currentTimeMillis()), null))
            segments.add(Segment(2, hala_szrenica, szrenica, 3, Date(System.currentTimeMillis()), null))
            segments.add(Segment(3, mokra_prz, szrenica, 2, Date(System.currentTimeMillis()), null))
            segments.add(Segment(4, szrenica, mokra_prz, 1, Date(System.currentTimeMillis()), null))
            segments.add(Segment(5, mokra_prz, labski, 2, Date(System.currentTimeMillis()), null))
            segments.add(Segment(6, labski, mokra_prz, 3, Date(System.currentTimeMillis()), null))

            initializedSegments = true
        }
    }
}