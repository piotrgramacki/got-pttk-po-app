package com.dratwscode.poapp.admin.model

import java.sql.Date

class Segment(val id: Int, val begin: Spot?, val end: Spot?, val points: Int, val startDate: Date, val endDate: Date?) {
    fun beginSpot() : String = begin!!.name
    fun endSpot() : String = end!!.name

    fun getSubGroup() : SubGroup = begin!!.subGroup
    fun getSubGroupString() : String = begin!!.subGroupString

    fun getGroup() : Group = begin!!.group
    fun getGroupString() : String = begin!!.groupString
}