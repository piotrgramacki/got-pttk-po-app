package com.dratwscode.poapp.admin.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.dratwscode.poapp.admin.viewmodels.SpotsViewModel
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.dratwscode.poapp.R
import com.dratwscode.poapp.admin.viewmodels.SegmentsViewModel
import com.dratwscode.poapp.admin.model.Spot
import kotlinx.android.synthetic.main.admin_activity_segment_add.*


class SegmentAddActivity : AppCompatActivity() {


    private lateinit var viewModel: SegmentsViewModel
    private lateinit var spotsViewModel: SpotsViewModel
    private lateinit var spotsList: List<Spot>
    private lateinit var endSpotsList: List<Spot>
    var chosenStartSpot: Spot? = null
    var chosenEndSpot: Spot? = null
    var chosenFirstWeight: Int? = null
    var chosenSecondWeight: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_activity_segment_add)
        viewModel = ViewModelProviders.of(this).get(SegmentsViewModel::class.java)
        spotsViewModel = ViewModelProviders.of(this).get(SpotsViewModel::class.java)

        initAutoCompleteStartSpotTextView()
        initAutoCompleteEndSpotTextView()
        initFirstWeightTextView()
        initSecondWeightTextView()
        initCancelButton()
        initSubmitButton()

    }

    private fun initAutoCompleteStartSpotTextView() {

        spotsList = spotsViewModel.getDataset()
        val custAdapter = SpotsAdapter(
            this, android.R.layout.simple_dropdown_item_1line,
            spotsList as java.util.ArrayList<Spot>
        )
        a_start_point_text_view.threshold = 1
        a_start_point_text_view.setAdapter<SpotsAdapter>(custAdapter)
        a_start_point_text_view.setTextColor(Color.BLACK)
        a_start_point_text_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                for (spot in spotsList) {
                    if (spot.name + ", " + spot.subGroup.name == p0.toString()) {
                        a_end_spot_name.visibility = View.VISIBLE
                        a_end_point_text_view.visibility = View.VISIBLE
                        enter_start_correct_error_text.visibility = View.INVISIBLE
                        chosenStartSpot = spot
                        val adapter = a_end_point_text_view.adapter as SpotsAdapter
                        adapter.filterSpots(spot)
                        break
                    } else {
                        a_end_spot_name.visibility = View.INVISIBLE
                        a_end_point_text_view.visibility = View.INVISIBLE
                        enter_end_correct_error_text.visibility = View.INVISIBLE
                        enter_start_correct_error_text.visibility = View.VISIBLE
                        chosenStartSpot = null
                        val adapter = a_end_point_text_view.adapter as SpotsAdapter
                        if (!adapter.getResetd()) {
                            adapter.resetSpots(spotsViewModel.getDataset())
                        }
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun initAutoCompleteEndSpotTextView() {
        endSpotsList = spotsViewModel.getDataset()
        val custAdapter = SpotsAdapter(
            this, android.R.layout.simple_dropdown_item_1line,
            endSpotsList as java.util.ArrayList<Spot>
        )
        a_end_point_text_view.threshold = 1
        a_end_point_text_view.setAdapter<SpotsAdapter>(custAdapter)
        a_end_point_text_view.setTextColor(Color.BLACK)
        a_end_point_text_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                for (spot in endSpotsList) {
                    if (spot.name + ", " + spot.subGroup.name == p0.toString() && chosenStartSpot != null && spot.name != chosenStartSpot?.name) {
                        a_enter_weights_text_view.visibility = View.VISIBLE
                        a_first_weight_label.visibility = View.VISIBLE
                        a_first_weight.visibility = View.VISIBLE
                        a_second_weight_label.visibility = View.VISIBLE
                        a_second_weight.visibility = View.VISIBLE
                        enter_end_correct_error_text.visibility = View.INVISIBLE
                        a_first_weight_label.text = chosenStartSpot?.name + " - " + spot.name
                        a_second_weight_label.text = spot.name + " - " + chosenStartSpot?.name
                        chosenEndSpot = spot
                        break
                    } else {
                        a_enter_weights_text_view.visibility = View.INVISIBLE
                        a_first_weight_label.visibility = View.INVISIBLE
                        a_first_weight.visibility = View.INVISIBLE
                        a_second_weight_label.visibility = View.INVISIBLE
                        a_second_weight.visibility = View.INVISIBLE
                        enter_end_correct_error_text.visibility = View.VISIBLE
                        chosenEndSpot = null
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun initFirstWeightTextView() {
        a_first_weight.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val value = viewModel.getValueFromEditable(p0)
                if (value >= 0) {
                    if (chosenSecondWeight != null) {
                        submit_button.visibility = View.VISIBLE
                    } else {
                        submit_button.visibility = View.INVISIBLE
                    }
                    chosenFirstWeight = value
                } else {
                    chosenFirstWeight = null
                    submit_button.visibility = View.INVISIBLE
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun initSecondWeightTextView() {
        a_second_weight.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                val value = viewModel.getValueFromEditable(p0)
                if (value >= 0) {
                    if (chosenFirstWeight != null) {
                        submit_button.visibility = View.VISIBLE
                    } else {
                        submit_button.visibility = View.INVISIBLE
                    }
                    chosenSecondWeight = value
                } else {
                    chosenSecondWeight = null
                    submit_button.visibility = View.INVISIBLE
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun initCancelButton() {
        cancel_button.setOnClickListener {
            finish()
        }
    }

    private fun initSubmitButton() {
        submit_button.setOnClickListener {
            if (chosenStartSpot != null && chosenEndSpot != null && chosenFirstWeight != null && chosenSecondWeight != null) {
                viewModel.postNewSegment(
                    chosenStartSpot!!, chosenEndSpot!!, chosenFirstWeight!!,
                    chosenSecondWeight!!
                )
                finish()
            } else {
                Toast.makeText(applicationContext, "Can't post data", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
