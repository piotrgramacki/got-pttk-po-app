package com.dratwscode.poapp.admin.model

import com.dratwscode.poapp.admin.model.Badge

class Book(
    val id: Int,
    val badge: Badge,
    val user: User?,
    var points: Int,
    var isActive: Boolean) {

    val badgeName : String = badge.name
}