package com.dratwscode.poapp.admin.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.dratwscode.poapp.R
import com.dratwscode.poapp.admin.viewmodels.SegmentsViewModel
import com.dratwscode.poapp.admin.model.Segment
import kotlinx.android.synthetic.main.admin_activity_segment_edit.*
import java.sql.Date

class SegmentEditActivity : AppCompatActivity() {

    private lateinit var viewModel: SegmentsViewModel
    private var date: Int = 0
    private var month: Int = 0
    private var year: Int = 0

    private var segmentID: Int = 0
    private var segmentStartSpot: String? = null
    private var segmentEndSpot: String? = null

    private var newWeight: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_activity_segment_edit)
        viewModel = ViewModelProviders.of(this).get(SegmentsViewModel::class.java)
        segmentID = intent.getIntExtra("segmentID", 0)
        segmentStartSpot = intent.getStringExtra("segmentStartSpot")
        segmentEndSpot = intent.getStringExtra("segmentEndSpot")
        initButtonsOnDatePicker()
        initButtonsOnWeight()


    }

    private fun initButtonsOnDatePicker() {
        a_date_submit_button.setOnClickListener {
            date = a_date_picker.dayOfMonth
            month = a_date_picker.month
            year = a_date_picker.year
            a_date_submit_button.visibility = View.GONE
            a_date_cancel_button.visibility = View.GONE
            a_date_picker.visibility = View.GONE
            a_date_picker_text_view.visibility = View.GONE
            a_new_weight.visibility = View.VISIBLE
            a_weight_label.visibility = View.VISIBLE
            a_weight_submit_button.visibility = View.VISIBLE
            a_weight_cancel_button.visibility = View.VISIBLE
        }

        a_date_cancel_button.setOnClickListener {
            finish()
        }

    }


    private fun initButtonsOnWeight() {
        a_weight_submit_button.setOnClickListener {
            val weight = a_new_weight.text
            newWeight = viewModel.getValueFromEditable(weight)

            if (newWeight!! >= 0) {
                var newSegment = Segment(segmentID, null, null, newWeight!!, Date(System.currentTimeMillis()), null)
                viewModel.updateSegment(newSegment)
                finish()
            } else {
                a_weight_error_text.visibility = View.VISIBLE
            }
        }

        a_weight_cancel_button.setOnClickListener {
            finish()
        }
    }
}
