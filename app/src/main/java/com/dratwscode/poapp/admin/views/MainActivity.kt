package com.dratwscode.poapp.admin.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.dratwscode.poapp.R
import androidx.navigation.ui.setupActionBarWithNavController
import kotlinx.android.synthetic.main.admin_activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_activity_main)

        setupToolbar()

        setupNavigation()
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(findNavController(R.id.a_main_nav_host), a_main_drawerLayout)
    }

    override fun onBackPressed() {
        if (a_main_drawerLayout.isDrawerOpen(GravityCompat.START)) {
            a_main_drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(a_main_toolbar)
    }

    private fun setupNavigation() {
        val navController = findNavController(R.id.a_main_nav_host)

        setupActionBarWithNavController(navController)

        a_main_navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            a_main_drawerLayout.closeDrawers()
            true
        }

        NavigationUI.setupWithNavController(a_main_navigationView, navController)
    }


}