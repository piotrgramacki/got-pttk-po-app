package com.dratwscode.poapp.admin.viewmodels

import android.text.Editable
import androidx.lifecycle.ViewModel
import com.dratwscode.poapp.communication.entities.OdcinekTO
import com.dratwscode.poapp.communication.requesters.impl.admin.SegmentsAdminRequester
import com.dratwscode.poapp.admin.model.Segment
import com.dratwscode.poapp.admin.model.Spot
import com.dratwscode.poapp.admin.viewmodels.mappers.Mapper
import java.sql.Date

class SegmentsViewModel : ViewModel() {

    val requester = SegmentsAdminRequester.getInstance()

    fun getDataset(): List<Segment> {
        val segmentTOList = requester.getAll
        return segmentTOList.map { odcinekTO -> Mapper.segmentFromTO(odcinekTO) }
    }

    fun postNewSegment(startSpot: Spot, endSpot: Spot, firstWeight : Int, secondWeight : Int){
        var startSpotTO = Mapper.spotToTO(startSpot)
        var endspotTO = Mapper.spotToTO(endSpot)
        val firstSegment = OdcinekTO(0,startSpotTO, endspotTO, firstWeight, Date(System.currentTimeMillis()),null)
        val secondSegment = OdcinekTO(0,endspotTO, startSpotTO, secondWeight, Date(System.currentTimeMillis()),null)
        requester.post(firstSegment)
        requester.post(secondSegment)
    }

    fun updateSegment(newSegment: Segment) {
        requester.update(Mapper.segmentToTO(newSegment))
    }

    fun getValueFromEditable(editable: Editable?): Int {
        val value: Int
        if (editable.toString() != "") {
            value = Integer.parseInt(editable.toString())
        } else {
            value = -1
        }
        return value
    }
}
