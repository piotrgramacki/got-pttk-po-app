package com.dratwscode.poapp.admin.viewmodels.mappers

import com.dratwscode.poapp.admin.model.Badge
import com.dratwscode.poapp.communication.entities.*
import com.dratwscode.poapp.admin.model.*

object Mapper {
    fun groupFromTO(from: GrupaGorskaTO): Group {
        return Group(id = from.id, name = from.nazwa)
    }

    fun groupToTO(to: Group): GrupaGorskaTO {
        return GrupaGorskaTO(id = to.id, nazwa = to.name)
    }

    fun subGroupFromTO(from: PodgrupaTO): SubGroup {
        return SubGroup(id = from.id, group = groupFromTO(from.grupa), name = from.nazwa, code = from.kod)
    }

    fun subGroupToTO(to: SubGroup): PodgrupaTO {
        return PodgrupaTO(id = to.id, nazwa = to.name, kod = to.code, grupa = groupToTO(to.group))
    }

    fun spotFromTO(from: PunktTO): Spot {
        return Spot(
            id = from.id,
            name = from.nazwa,
            startDate = from.data_rozpoczecia,
            endDate = from.data_zakonczenia,
            subGroup = subGroupFromTO(from.podgrupa)
        )
    }


    fun spotToTO(to: Spot): PunktTO {
        return PunktTO(
            id = to.id,
            nazwa = to.name,
            data_rozpoczecia = to.startDate,
            data_zakonczenia = to.endDate,
            podgrupa = subGroupToTO(to.subGroup)
        )
    }

    fun segmentFromTO(from: OdcinekTO): Segment {
        return Segment(
            id = from.id,
            points = from.punkty,
            startDate = from.data_rozpoczecia,
            endDate = from.data_zakonczenia,
            begin = spotFromTO(from.punktpoczatkowy!!),
            end = spotFromTO(from.punkt_koncowy!!)
        )
    }

    fun segmentToTO(to: Segment): OdcinekTO {
        return OdcinekTO(
            id = to.id,
            punktpoczatkowy = if (to.begin !=null) {
                spotToTO(to.begin)} else {null},
            punkt_koncowy = if (to.end !=null) {
                spotToTO(to.end)} else {null},
            punkty = to.points,
            data_rozpoczecia = to.startDate,
            data_zakonczenia = to.endDate
        )
    }

    fun badgeFromTO(from: StopienTO): Badge {
        return Badge(
            id = from.id,
            name = from.nazwa_stopnia.nazwa,
            nameId = from.nazwa_stopnia.id,
            points = from.wymagana_liczba_punktow
        )
    }

    fun badgeToTO(from: Badge): StopienTO {
        return StopienTO(
            id = from.id,
            nazwa_stopnia = NazwaStopniaTO(
                id = from.nameId,
                nazwa = from.name
            ),
            wymagana_liczba_punktow = from.points
        )
    }

    fun bookFromTO(from: KsiazeczkaTO): Book {
        return Book(
            id = from.id,
            user = userFromTO(from.uzytkownik),
            badge = badgeFromTO(from = from.stopien),
            points = from.stan_punktow,
            isActive = from.czy_aktywna
        )
    }

    fun userFromTO(from: UzytkownikTO): User {
        return User(
            id = from.id,
            login = from.login!!,
            haslo = from.haslo!!,
            dateBirth = from.data_ur!!,
            joinDate = from.data_wst!!,
            name = from.imie!!,
            surname = from.nazwisko!!,
            isAdmin = from.czy_administrator,
            isTourist = from.czy_turysta
        )
    }

    fun userToTO(to: User): UzytkownikTO {
        return UzytkownikTO(
            id = to.id,
            login = to.login,
            haslo = to.haslo,
            data_ur = to.dateBirth,
            data_wst = to.joinDate,
            imie = to.name,
            nazwisko = to.surname,
            czy_administrator = to.isAdmin,
            czy_turysta = to.isTourist
        )
    }

    fun bookToTO(from: Book): KsiazeczkaTO {
        return KsiazeczkaTO(
            id = from.id,
            stopien = badgeToTO(from = from.badge),
            uzytkownik = userToTO(from.user!!),
            stan_punktow = from.points,
            czy_aktywna = from.isActive
        )
    }

    fun tripFromTO(from: TrasaTO): Trip {
        return Trip(
            id = from.id,
            book = bookFromTO(from.ksiazeczka!!),
            date = from.data,
            points = from.suma_pkt
        )
    }

    fun tripToTO(to: Trip): TrasaTO {
        return TrasaTO(
            id = to.id,
            ksiazeczka = bookToTO(to.book),
            data = to.date,
            suma_pkt = to.points
        )
    }
}