package com.dratwscode.poapp.admin.views

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dratwscode.poapp.R
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.dratwscode.poapp.admin.model.Segment

class SegmentsAdapter(private val context: Context, private val dataset: List<Segment>) : RecyclerView.Adapter<SegmentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context).inflate(
            R.layout.admin_segments_item,
            parent,
            false
        ) as ConstraintLayout

        return ViewHolder(constraintLayout)
    }

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.startTV.text = dataset[position].begin?.name
        holder.endTV.text = dataset[position].end?.name
        val points = dataset[position].points
        holder.ptsTV.text = String.format("%d pkt", points)
        holder.groupCodeTV.text = dataset[position].end?.subGroup?.code
        holder.editButton.setOnClickListener {
            val intent = Intent(context, SegmentEditActivity::class.java)
            intent.putExtra("segmentID", dataset[position].id)
            intent.putExtra("segmentStartSpot", dataset[position].begin?.name)
            intent.putExtra("segmentEndSpot", dataset[position].end?.name)
            context.startActivity(intent)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val startTV = itemView.findViewById<TextView>(R.id.a_segments_item_start)!!
        val endTV = itemView.findViewById<TextView>(R.id.a_segments_item_end)!!
        val ptsTV = itemView.findViewById<TextView>(R.id.a_segments_item_pts)!!
        val groupCodeTV = itemView.findViewById<TextView>(R.id.a_segments_item_group)!!
        val editButton = itemView.findViewById<ImageView>(R.id.a_segments_item_arrow)!!
    }
}