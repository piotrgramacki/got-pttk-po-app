package com.dratwscode.poapp.communication.requesters.impl.admin

import com.dratwscode.poapp.communication.entities.PunktTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

//@TODO object instead of class singleton
class SpotsAdminRequester private constructor() : Requester<PunktTO>, AbstractRequster() {
    private val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    override val getAll: List<PunktTO>
        get() {
            val json = get("spots/all")
            val listType = object : TypeToken<List<PunktTO>>() { }.type
            return gson.fromJson(json, listType)
        }

    override fun post(objectToPost: PunktTO) {
        val data = gson.toJson(objectToPost, PunktTO::class.java)
        post("spots/save", data)
    }

    companion object {

        private var instance: SpotsAdminRequester? = null

        fun getInstance(): SpotsAdminRequester {
            if (instance == null) {
                instance =
                        SpotsAdminRequester()
            }
            return instance as SpotsAdminRequester
        }
    }


}
