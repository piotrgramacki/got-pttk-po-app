package com.dratwscode.poapp.communication.entities

class KsiazeczkaTO(
    val id: Int,
    val stopien: StopienTO,
    val uzytkownik: UzytkownikTO,
    var stan_punktow: Int,
    var czy_aktywna: Boolean
)
