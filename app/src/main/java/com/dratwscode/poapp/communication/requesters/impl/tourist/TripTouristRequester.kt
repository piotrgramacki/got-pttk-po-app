package com.dratwscode.poapp.communication.requesters.impl.tourist

import com.dratwscode.poapp.communication.entities.TrasaTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class TripTouristRequester private constructor() : Requester<TrasaTO>, AbstractRequster() {
    private val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    override fun post(objectToPost: TrasaTO) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val getAll: List<TrasaTO>
        get() {
            val json = get("trips/all")
            val listType = object : TypeToken<List<TrasaTO>>() {}.type
            return gson.fromJson(json, listType)
        }

    fun post(objectToPost: TrasaTO, bookID: Int) : Int {
        val data = gson.toJson(objectToPost, TrasaTO::class.java)
        return post("trips/save/$bookID", data)
    }

    fun getAllById(bookID: Int): List<TrasaTO> {
        val json = get("trips/$bookID")
        val listType = object : TypeToken<List<TrasaTO>>() {}.type
        return gson.fromJson(json, listType)
    }

    companion object {

        private var instance: TripTouristRequester? = null

        fun getInstance(): TripTouristRequester {
            if (instance == null) {
                instance =
                        TripTouristRequester()
            }
            return instance as TripTouristRequester
        }
    }
}
