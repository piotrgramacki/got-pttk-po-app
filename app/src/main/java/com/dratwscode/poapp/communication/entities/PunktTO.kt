package com.dratwscode.poapp.communication.entities

import java.sql.Date

class PunktTO(
    val id: Int,
    val podgrupa: PodgrupaTO,
    val nazwa: String,
    val data_rozpoczecia: Date,
    var data_zakonczenia: Date? = null)
