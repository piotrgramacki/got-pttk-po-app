package com.dratwscode.poapp.communication.requesters.impl.admin

import com.dratwscode.poapp.communication.entities.OdcinekTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class SegmentsAdminRequester private constructor() : Requester<OdcinekTO>, AbstractRequster() {
    private val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    override val getAll: List<OdcinekTO>
        get() {
            val json = get("segments/all")
            val listType = object : TypeToken<List<OdcinekTO>>() {}.type
            return gson.fromJson(json, listType)
        }

    override fun post(objectToPost: OdcinekTO) {
        val data = gson.toJson(objectToPost, OdcinekTO::class.java)
        post("segments/save", data)
    }

    fun update(newSegment: OdcinekTO) {
        val data = gson.toJson(newSegment, OdcinekTO::class.java)
        post("segments/update", data)
    }

    companion object {

        private var instance: SegmentsAdminRequester? = null

        fun getInstance(): SegmentsAdminRequester {
            if (instance == null) {
                instance =
                        SegmentsAdminRequester()
            }
            return instance as SegmentsAdminRequester
        }
    }
}