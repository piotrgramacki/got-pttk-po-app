package com.dratwscode.poapp.communication.requesters

import android.os.AsyncTask
import com.dratwscode.poapp.communication.requesters.exceptions.RequestException
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL

open class AbstractRequster {

    var pcIP: String = "192.168.0.248"

    fun get(urlSuffix: String): String {
        var asyncTask = object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg params: Void?): String {
                val url = URL("http://$pcIP:8090/$urlSuffix")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "GET"
                conn.setRequestProperty("Accept", "application/json")
                if (conn.responseCode != 200) {
                    throw RequestException("Failed to get all objects from: " + url + ", HTTP error code : " + conn.responseCode)
                }
                val br = BufferedReader(InputStreamReader(conn.inputStream))
                var data = ""
                var line: String? = ""
                while (line != null) {
                    line = br.readLine()
                    if (line != null) {
                        data = data + line + "\n"
                    }
                }
                br.close()
                conn.disconnect()
                return data
            }
        }
        return asyncTask.execute().get()
    }

    fun post(urlSuffix: String, jsonData: String): Int {
        var asyncTask = object : AsyncTask<Void, Void, Int>() {
            override fun doInBackground(vararg params: Void?): Int {
                val url = URL("http://$pcIP:8090/$urlSuffix")
                val conn = url.openConnection() as HttpURLConnection
                conn.doOutput = true
                conn.requestMethod = "POST"
                conn.setRequestProperty("Content-Type", "application/json")
                conn.setRequestProperty("Accept", "application/json")
                val dataOutpuStream: OutputStream? = conn.outputStream
                dataOutpuStream?.write(jsonData.toByteArray(Charsets.UTF_8))
                conn.connect()
                if (conn.responseCode != 200) {
                    throw RequestException("Failed to post segmentsData to: " + url + ", HTTP error code : " + conn.responseCode)
                }


                val br = BufferedReader(InputStreamReader(conn.inputStream))
                var data = ""
                var line: String? = ""
                while (line != null) {
                    line = br.readLine()
                    if (line != null) {
                        data = data + line + "\n"
                    }
                }
                br.close()
                conn.disconnect()
                if (data != "") {
                    return Integer.parseInt(data.trim())
                }else{
                    return 0
                }
            }
        }
        return asyncTask.execute().get()
    }
}
