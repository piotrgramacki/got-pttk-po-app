package com.dratwscode.poapp.communication.requesters.impl.tourist

import com.dratwscode.poapp.communication.entities.OdcinekTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
class SegmentsTouristRequester private constructor() : Requester<OdcinekTO>, AbstractRequster() {
    private val gson: Gson

    init {
        gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
    }

    override val getAll: List<OdcinekTO>
        get() {
            val json = get("segments/all/tourist")
            val listType = object : TypeToken<List<OdcinekTO>>() {}.type
            return gson.fromJson(json, listType)
        }

    override fun post(objectToPost: OdcinekTO) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getBySpot(spotId: Int): List<OdcinekTO> {
        val data = get("segments/spots/$spotId")
        val listType = object : TypeToken<List<OdcinekTO>>() { }.type
        return gson.fromJson(data, listType)
    }

    companion object {

        private var instance: SegmentsTouristRequester? = null

        fun getInstance(): SegmentsTouristRequester {
            if (instance == null) {
                instance =
                        SegmentsTouristRequester()
            }
            return instance as SegmentsTouristRequester
        }
    }
}