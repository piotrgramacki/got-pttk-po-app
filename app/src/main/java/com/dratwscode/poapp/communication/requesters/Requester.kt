package com.dratwscode.poapp.communication.requesters

interface Requester<T> {

    val getAll: List<T>

    fun post(objectToPost: T)
}
