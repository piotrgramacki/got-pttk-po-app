package com.dratwscode.poapp.communication.entities

class OdcinekTrasyTO {

    var id: Int = 0
    var trasa: TrasaTO? = null
    var odcinek: OdcinekTO? = null
    var poczatek: String? = null
    var koniec: String? = null
    var przewyzszenie: Int? = null
    var dlugosc: Int? = null

    constructor() {}

    constructor(
        id: Int,
        trasa: TrasaTO,
        odcinek: OdcinekTO,
        poczatek: String,
        koniec: String,
        przewyzszenie: Int?,
        dlugosc: Int?
    ) {
        this.id = id
        this.trasa = trasa
        this.odcinek = odcinek
        this.poczatek = poczatek
        this.koniec = koniec
        this.przewyzszenie = przewyzszenie
        this.dlugosc = dlugosc
    }
}
