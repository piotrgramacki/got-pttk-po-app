package com.dratwscode.poapp.communication.entities

class StopienTO(
    val id: Int,
    val nazwa_stopnia: NazwaStopniaTO,
    val wymagana_liczba_punktow: Int)
