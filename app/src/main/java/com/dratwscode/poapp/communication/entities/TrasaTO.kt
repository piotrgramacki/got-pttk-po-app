package com.dratwscode.poapp.communication.entities

import java.sql.Date

class TrasaTO(val id: Int, val ksiazeczka: KsiazeczkaTO?, val data: Date, val suma_pkt: Int)
