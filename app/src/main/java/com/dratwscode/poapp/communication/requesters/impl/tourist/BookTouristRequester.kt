package com.dratwscode.poapp.communication.requesters.impl.tourist

import android.os.AsyncTask
import com.dratwscode.poapp.communication.entities.KsiazeczkaTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.dratwscode.poapp.communication.requesters.exceptions.RequestException
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.net.HttpURLConnection
import java.net.URL

class BookTouristRequester private constructor() : Requester<KsiazeczkaTO>, AbstractRequster() {
    private val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    override val getAll: List<KsiazeczkaTO>
        get() {
            val json = get("books/all")
            val listType = object : TypeToken<List<KsiazeczkaTO>>() {}.type
            return gson.fromJson(json, listType)
        }

    override fun post(objectToPost: KsiazeczkaTO) {
        val data = gson.toJson(objectToPost, KsiazeczkaTO::class.java)
        post("books/save", data)
    }

    fun getAllById(userID: Int): List<KsiazeczkaTO> {
        val json = get("books/$userID")
        val listType = object : TypeToken<List<KsiazeczkaTO>>() {}.type
        return gson.fromJson(json, listType)
    }

    fun updateBook(bookID: Int, points: Int) {
        var asyncTask = object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg params: Void?): Void? {
                val url = URL("http://$pcIP:8090/books/$bookID/$points")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "POST"
                conn.setRequestProperty("Accept", "application/json")
                if (conn.responseCode != 200) {
                    throw RequestException("Failed to get all objects from: " + url + ", HTTP error code : " + conn.responseCode)
                }
                conn.disconnect()
                return null
            }
        }
        asyncTask.execute()
    }

    companion object {

        private var instance: BookTouristRequester? = null

        fun getInstance(): BookTouristRequester {
            if (instance == null) {
                instance =
                        BookTouristRequester()
            }
            return instance as BookTouristRequester
        }
    }


}