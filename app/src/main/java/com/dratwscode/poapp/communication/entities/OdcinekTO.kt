package com.dratwscode.poapp.communication.entities

import java.sql.Date

class OdcinekTO(
    val id: Int = 0,
    var punktpoczatkowy: PunktTO?,
    var punkt_koncowy: PunktTO?,
    var punkty: Int,
    val data_rozpoczecia: Date,
    var data_zakonczenia: Date? = null)
