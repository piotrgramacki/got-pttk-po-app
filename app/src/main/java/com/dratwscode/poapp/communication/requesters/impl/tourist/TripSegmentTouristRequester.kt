package com.dratwscode.poapp.communication.requesters.impl.tourist

import android.os.AsyncTask
import com.dratwscode.poapp.communication.entities.OdcinekTrasyTO
import com.dratwscode.poapp.communication.requesters.AbstractRequster
import com.dratwscode.poapp.communication.requesters.Requester
import com.dratwscode.poapp.communication.requesters.exceptions.RequestException
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.net.HttpURLConnection
import java.net.URL

class TripSegmentTouristRequester private constructor() : Requester<OdcinekTrasyTO>, AbstractRequster() {
    private val gson: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    override val getAll: List<OdcinekTrasyTO>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun post(objectToPost: OdcinekTrasyTO) {
        val data = gson.toJson(objectToPost, OdcinekTrasyTO::class.java)
        post("tripSegments/save", data)
    }

    fun post(tripID: Int, segmentID: Int, position: Int) {
        var asyncTask = object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg params: Void?): Void? {
                val url = URL("http://$pcIP:8090/tripSegments/save/$tripID/$segmentID/$position")
                val conn = url.openConnection() as HttpURLConnection

                conn.setRequestProperty("Content-Type", "application/json")
                conn.requestMethod = "POST"

                if (conn.responseCode != 200) {
                    throw RequestException("Failed to post segmentsData to: " + url + ", HTTP error code : " + conn.responseCode)
                }
                conn.disconnect()
                return null
            }
        }
        asyncTask.execute()
    }


    companion object {

        private var instance: TripSegmentTouristRequester? = null

        fun getInstance(): TripSegmentTouristRequester {
            if (instance == null) {
                instance =
                        TripSegmentTouristRequester()
            }
            return instance as TripSegmentTouristRequester
        }
    }
}