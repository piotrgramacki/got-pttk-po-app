package com.dratwscode.poapp.communication.entities

import java.sql.Date

class UzytkownikTO {

    var id: Int = 0
    var login: String? = null
    var haslo: String? = null
    var imie: String? = null
    var nazwisko: String? = null
    var data_ur: Date? = null
    var data_wst: Date? = null
    var czy_administrator: Boolean = false
    var czy_turysta: Boolean = false

    constructor() {}

    constructor(
        id: Int,
        login: String,
        haslo: String,
        imie: String,
        nazwisko: String,
        data_ur: Date,
        data_wst: Date,
        czy_administrator: Boolean,
        czy_turysta: Boolean
    ) {
        this.id = id
        this.login = login
        this.haslo = haslo
        this.imie = imie
        this.nazwisko = nazwisko
        this.data_ur = data_ur
        this.data_wst = data_wst
        this.czy_administrator = czy_administrator
        this.czy_turysta = czy_turysta
    }
}
