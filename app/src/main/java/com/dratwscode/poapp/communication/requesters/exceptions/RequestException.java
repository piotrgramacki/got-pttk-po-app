package com.dratwscode.poapp.communication.requesters.exceptions;

public class RequestException extends Exception {

    public RequestException(String message, Exception ex) {
        super(message);
        this.initCause(ex.getCause());
    }

    public RequestException(String message) {
        super(message);
    }
}
