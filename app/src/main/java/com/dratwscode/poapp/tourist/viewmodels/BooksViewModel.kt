package com.dratwscode.poapp.tourist.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dratwscode.poapp.admin.model.Badge
import com.dratwscode.poapp.communication.requesters.impl.tourist.BookTouristRequester
import com.dratwscode.poapp.tourist.model.*
import com.dratwscode.poapp.tourist.viewmodels.mappers.Mapper
import kotlin.math.max

class BooksViewModel(val requester: BookTouristRequester = BookTouristRequester.getInstance()) : ViewModel() {
    private val booksList: MutableLiveData<MutableList<Book>> = MutableLiveData()
    private lateinit var currentUser: User
//    val requester = BookTouristRequester.getInstance()

    init {
        load()
    }

    val books: LiveData<MutableList<Book>> = booksList

    fun canAdd(): Boolean = !booksList.value!!.fold(false) { sum, el -> sum || el.isActive }

    fun getLastBadgeId() : Int = booksList.value?.fold(0) {acc, it -> max(acc, it.badge.id)} ?: 0

    fun getOverflow() : Int {
        val last = booksList.value?.find { it.badge.id == getLastBadgeId() }

        val over = last?.let { last.points - last.badge.points } ?: 0

        return max(over, 0)
    }

    fun saveBook(badgeId: Int, points: Int) {
        val book = Book(
            0,
            Badge(badgeId, Mockups.badgesData[badgeId].first.name, badgeId, Mockups.badgesData[badgeId].first.points),
            currentUser,
            points,
            true)
        upload(book)
        booksList.value?.add(book)
        booksList.value = booksList.value
//        load()
    }

    private fun load() {
        val result = requester.getAll

        val list = mutableListOf<Book>()

        if (!result.isEmpty())
            currentUser = Mapper.userFromTO(result[0].uzytkownik)

        for (el in result.filter { it.uzytkownik.id == currentUser.id })
            list.add(Mapper.bookFromTO(el))

        booksList.value = list
    }

    private fun upload(book: Book) {
        val bookTO = Mapper.bookToTO(book)
        requester.post(bookTO)
    }
}
