package com.dratwscode.poapp.tourist.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import com.dratwscode.poapp.R
import kotlinx.android.synthetic.main.tourist_fragment_books.*

class BooksFragment : Fragment() {
    companion object {
        fun newInstance() = BooksFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tourist_fragment_books, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//         TODO: Use the ViewModel

        t_books_pager.adapter = PagerAdapter(childFragmentManager)

        t_books_tab_layout.setupWithViewPager(t_books_pager)

    }

    private inner class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> BooksListFragment.newInstance(false)
                else -> BooksListFragment.newInstance(true)
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> "NIEAKTYWNE"
                else -> "AKTYWNE"
            }
        }

    }
}
