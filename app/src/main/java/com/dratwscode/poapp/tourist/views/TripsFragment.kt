package com.dratwscode.poapp.tourist.views

import android.icu.text.DateFormat
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.model.Trip
import com.dratwscode.poapp.tourist.viewmodels.TripViewModel
import kotlinx.android.synthetic.main.tourist_fragment_trips.*

class TripsFragment : Fragment() {
    private var bookID : Int = 0
    private val adapter = Adapter(listOf())

    companion object {
        fun newInstance() = TripsFragment()
    }

    private lateinit var viewModel: TripViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tourist_fragment_trips, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run { viewModel = ViewModelProviders.of(this).get(TripViewModel::class.java) }

        setupRecycler()
        setupFAB()
        setupData()
    }

    private fun setupData() {
        bookID = arguments?.let { TripsFragmentArgs.fromBundle(it).bookID } ?: 0

        adapter.dataSet = viewModel.getTrips(bookID)
    }

    private fun setupFAB() {
        t_trips_fab.setOnClickListener {
            val destination = TripsFragmentDirections.actionTripsFragmentToAddTripFragment(bookID)
            findNavController().navigate(destination)
        }
    }

    private fun setupRecycler() {
        t_trips_recycler.layoutManager = LinearLayoutManager(context)
        t_trips_recycler.adapter = adapter
    }

    private inner class Adapter(var dataSet: List<Trip>) : RecyclerView.Adapter<Adapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val layout = LayoutInflater.from(context).inflate(R.layout.tourist_trips_item, parent, false)

            return ViewHolder(layout)
        }

        override fun getItemCount(): Int = dataSet.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val date = dataSet[position].date
            holder.date.text = DateFormat.getInstance().format(date).substring(0, 10)
            holder.pts.text = getString(R.string.pts, dataSet[position].points)
        }

        private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val date = itemView.findViewById<TextView>(R.id.t_trips_item_date)
            val pts = itemView.findViewById<TextView>(R.id.t_trips_item_pts)
        }
    }

}
