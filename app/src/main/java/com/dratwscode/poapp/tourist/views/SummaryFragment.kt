package com.dratwscode.poapp.tourist.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.viewmodels.BooksViewModel


class SummaryFragment : Fragment() {
    private lateinit var viewModel: BooksViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tourist_fragment_summary, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

//        viewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java)
//
//        viewModel.getActive()?.let { book ->
//            t_summary_current_badge_text.text = book.badgeName
//            t_summary_current_points.text = "${book.points} pts"
//        }
    }

    override fun onResume() {
        super.onResume()

//        viewModel.getActive()?.let { book ->
//            t_summary_current_badge_text.text = book.badgeName
//            t_summary_current_points.text = "${book.points} pts"
//        }
    }


    companion object {
        fun newInstance() = SummaryFragment()
    }
}
