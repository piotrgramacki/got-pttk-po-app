package com.dratwscode.poapp.tourist.model

import java.sql.Date

class User(val id : Int, val login : String, val haslo : String,
           val name : String, val surname : String, val dateBirth : Date,
           val joinDate : Date, val isAdmin : Boolean, val isTourist : Boolean) {
}