package com.dratwscode.poapp.tourist.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.navigateUp
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.dratwscode.poapp.R
import kotlinx.android.synthetic.main.tourist_activity_main.*

class  MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tourist_activity_main)

        setSupportActionBar(t_main_toolbar)

        setupNavigation()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigateUp(findNavController(R.id.t_main_nav_host), t_main_drawerLayout)
    }

    override fun onBackPressed() {
        if (t_main_drawerLayout.isDrawerOpen(GravityCompat.START)) {
            t_main_drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun setupNavigation() {
        val navController = findNavController(R.id.t_main_nav_host)

        setupActionBarWithNavController(navController)

        t_main_navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            t_main_drawerLayout.closeDrawers()
            true
        }

        setupWithNavController(t_main_navigationView, navController)

        //TODO - use it to display drawer icon somehow
        val appBarConfiguration = AppBarConfiguration(navController.graph, t_main_drawerLayout)
        t_main_toolbar.setupWithNavController(navController, appBarConfiguration)
    }


}
