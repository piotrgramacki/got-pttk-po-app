package com.dratwscode.poapp.tourist.model

import com.dratwscode.poapp.R
import java.sql.Date

object Mockups {
    private var initializedSegments = false
    private var initializedBadges = false
    private val segments: MutableList<Segment> = mutableListOf()
    private val badges: MutableList<Pair<Badge, Int>> = mutableListOf()

    const val NUMBER_OF_BADGES = 7

    val segmentsData: MutableList<Segment>
        get() {
            if (!initializedSegments)
                initializeSegments()
            return segments
        }

    val badgesData: List<Pair<Badge, Int>>
        get() {
            if (!initializedBadges)
                initializeBadges()
            return badges
        }

    private fun initializeBadges() {
        if (!initializedBadges) {
            badges.add(Pair(Badge(0, "", 0, 0), R.drawable.ic_clear_black_24dp))
            badges.add(Pair(Badge(1, "\"W góry\" brązowa", 1, 15), R.drawable.w_gory_brazowa))
            badges.add(Pair(Badge(2, "\"W góry\" srebrna", 2, 40), R.drawable.w_gory_srebrna))
            badges.add(Pair(Badge(3, "\"W góry\" złota", 3, 40), R.drawable.w_gory_zlota))
            badges.add(Pair(Badge(4, "Popularna", 4, 60), R.drawable.popularna_image_view))
            badges.add(Pair(Badge(5, "Mała brązowa", 5, 120), R.drawable.mala_brazowa_image_view))
            badges.add(Pair(Badge(6, "Mała srebrna", 6, 360), R.drawable.mala_srebrna_image_view))
            badges.add(Pair(Badge(7, "Mała złota", 7, 720), R.drawable.mala_zlota_image_view))

            initializedBadges = true
        }
    }

    private fun initializeSegments() {
        if (!initializedSegments) {
            // groups
            val sudety = Group(1, "Sudety")

            //subgroups
            val karkonosze = SubGroup(1, "Karkonosze", "S.03", sudety)
            val izery = SubGroup(1, "Gory Izerskie", "S.01", sudety)

            //points
            val szrenica = Spot(0, "Szrenica", karkonosze, Date(System.currentTimeMillis()), null)
            val hala_szrenica = Spot(0, "Hala Szrenicka", karkonosze, Date(System.currentTimeMillis()), null)
            val mokra_prz = Spot(0, "Mokra Przelecz", karkonosze, Date(System.currentTimeMillis()), null)
            val labski =
                Spot(0, "Schr. PTTK Hala pod Labskim Szczytem", karkonosze, Date(System.currentTimeMillis()), null)

            //segments
            segments.add(Segment(1, szrenica, hala_szrenica, 1, Date(System.currentTimeMillis()), null))
            segments.add(Segment(2, hala_szrenica, szrenica, 3, Date(System.currentTimeMillis()), null))
            segments.add(Segment(3, mokra_prz, szrenica, 2, Date(System.currentTimeMillis()), null))
            segments.add(Segment(4, szrenica, mokra_prz, 1, Date(System.currentTimeMillis()), null))
            segments.add(Segment(5, mokra_prz, labski, 2, Date(System.currentTimeMillis()), null))
            segments.add(Segment(6, labski, mokra_prz, 3, Date(System.currentTimeMillis()), null))

            initializedSegments = true
        }
    }
}