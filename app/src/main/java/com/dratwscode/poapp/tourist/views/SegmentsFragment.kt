package com.dratwscode.poapp.tourist.views

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.model.Segment
import com.dratwscode.poapp.tourist.viewmodels.SegmentsViewModel
import kotlinx.android.synthetic.main.tourist_fragment_segments.*

class SegmentsFragment : Fragment() {

    companion object {
        fun newInstance() = SegmentsFragment()
    }

    private lateinit var viewModel: SegmentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.tourist_fragment_segments, container, false)
    }

    private fun initializeRecyclerView() {
        t_segments_recycler_view.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(activity)

            adapter = SegmentsAdapter(viewModel.getDataset())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run { viewModel = ViewModelProviders.of(this).get(SegmentsViewModel::class.java) }

        initializeRecyclerView()
        // TODO: Use the ViewModel
    }


    private inner class SegmentsAdapter(private val dataSet: List<Segment>) : RecyclerView.Adapter<SegmentsAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val constraintLayout = LayoutInflater.from(parent.context).inflate(
                R.layout.tourist_segments_item,
                parent,
                false
            ) as ConstraintLayout

            return ViewHolder(constraintLayout)
        }

        override fun getItemCount(): Int = dataSet.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.startTV.text = dataSet[position].beginSpot()
            holder.endTV.text = dataSet[position].endSpot()
            val points = dataSet[position].points
            holder.ptsTV.text = String.format("%d pkt", points)
            holder.groupCodeTV.text = dataSet[position].getSubGroup().code
        }

        private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val startTV = itemView.findViewById<TextView>(R.id.t_segments_item_start)!!
            val endTV = itemView.findViewById<TextView>(R.id.t_segments_item_end)!!
            val ptsTV = itemView.findViewById<TextView>(R.id.t_segments_item_pts)!!
            val groupCodeTV = itemView.findViewById<TextView>(R.id.t_segments_item_group)!!
        }
    }

}
