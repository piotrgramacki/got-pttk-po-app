package com.dratwscode.poapp.tourist.views


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.model.Book
import com.dratwscode.poapp.tourist.model.Mockups
import com.dratwscode.poapp.tourist.viewmodels.BooksViewModel
import kotlinx.android.synthetic.main.tourist_fragment_books_list.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_SHOW_ACTIVE = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [BooksListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class BooksListFragment : Fragment() {
    private var showActive: Boolean = true

    private lateinit var viewModel: BooksViewModel

    private val adapter: BookAdapter = BookAdapter(listOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            showActive = it.getBoolean(ARG_SHOW_ACTIVE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tourist_fragment_books_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.run { viewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java) }

        setupRecycler()
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.books.observe(this, Observer { list ->
            adapter.dataList = list.filter { showActive == it.isActive }
            adapter.notifyDataSetChanged()

            setupFAB()
        })
    }

    private fun setupRecycler() {
        val layoutManager = LinearLayoutManager(context)
        t_books_list_recycler_view.layoutManager = layoutManager

        t_books_list_recycler_view.adapter = adapter
    }

    private fun setupFAB() {
        if (showActive && viewModel.canAdd())
            t_books_list_fab.visibility = View.VISIBLE
        else
            t_books_list_fab.visibility = View.INVISIBLE

        t_books_list_fab.setOnClickListener {
            val destination = BooksFragmentDirections.actionBooksFragmentToAddBookFragment()
            findNavController().navigate(destination)
        }
    }

    private fun onBookClick(bookID: Int) {
        val destination = BooksFragmentDirections.actionBooksFragmentToTripsFragment(bookID)
        findNavController().navigate(destination)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param showAll - flag whether to show all books or not.
         * @return A new instance of fragment BooksListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(showActive: Boolean) =
            BooksListFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ARG_SHOW_ACTIVE, showActive)
                }
            }
    }

    private inner class BookAdapter(var dataList : List<Book>): RecyclerView.Adapter<BookAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val constraintLayout = LayoutInflater.from(parent.context).inflate(R.layout.tourist_books_list_item, parent, false) as ConstraintLayout

            return ViewHolder(constraintLayout)
        }

        override fun getItemCount(): Int {
            return dataList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = dataList[position].badgeName
            holder.pts.text = getString(R.string.pts, dataList[position].points)
            holder.image.setImageResource(Mockups.badgesData.firstOrNull { pair -> pair.first.id == dataList[position].badge.id}!!.second)
            if (dataList[position].isActive)
                holder.done.visibility = View.INVISIBLE
            else
                holder.done.visibility = View.VISIBLE

            holder.image.setOnClickListener {
                onBookClick(dataList[position].id)
            }
        }

        private inner class ViewHolder(itemView: ConstraintLayout) : RecyclerView.ViewHolder(itemView) {
            val image = itemView.findViewById<ImageView>(R.id.t_books_list_image)!!
            val name = itemView.findViewById<TextView>(R.id.t_books_list_badge_name)!!
            val pts = itemView.findViewById<TextView>(R.id.t_books_list_pts)!!
            val done = itemView.findViewById<ImageView>(R.id.t_books_list_finished_mark)!!
        }
    }
}
