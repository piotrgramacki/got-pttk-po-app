package com.dratwscode.poapp.tourist.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.model.Spot
import com.dratwscode.poapp.tourist.viewmodels.TripViewModel
import kotlinx.android.synthetic.main.tourist_fragment_add_trip.*
import java.text.DateFormat
import java.util.*

class AddTripFragment : Fragment() {
    private val dataAdapter: DataAdapter = DataAdapter(listOf())
    private val resultAdapter: ResultAdapter = ResultAdapter(listOf())

    private lateinit var viewModel : TripViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tourist_fragment_add_trip, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.run { viewModel = ViewModelProviders.of(this).get(TripViewModel::class.java) }

        val bookID = arguments?.let { TripsFragmentArgs.fromBundle(it).bookID } ?: 0

        setupData()

        setupResult()

        setupSummary()

        t_add_trip_confirm.setOnClickListener {
            viewModel.save(bookID)
            findNavController().popBackStack()
        }
    }

    private fun setupSummary() {
        t_add_trip_date.text = DateFormat.getInstance().format(Date()).substring(0, 10)
        t_add_trip_points.text = "0"
    }

    private fun setupResult() {
        t_add_trip_result_recycler.layoutManager = LinearLayoutManager(context)

        t_add_trip_result_recycler.adapter = resultAdapter

        resultAdapter.dataSet = viewModel.loadChosen()

        resultAdapter.notifyDataSetChanged()
    }

    private fun setupData() {
        t_add_trip_data_recycler.layoutManager = LinearLayoutManager(context)

        t_add_trip_data_recycler.adapter = dataAdapter

        dataAdapter.dataSet = viewModel.loadInitSegments()

        dataAdapter.notifyDataSetChanged()
    }

    private fun chooseSpot(spot: Spot) {
        val points = viewModel.chooseSpot(spot)

        t_add_trip_points.text = points.toString()

        dataAdapter.dataSet = viewModel.getNextSpots()

        resultAdapter.dataSet = viewModel.loadChosen()

        dataAdapter.notifyDataSetChanged()
        resultAdapter.notifyDataSetChanged()
    }

    private fun removeLast() {
        val pts = viewModel.removeSpot()

        t_add_trip_points.text = pts.toString()

        dataAdapter.dataSet = viewModel.getNextSpots()

        resultAdapter.dataSet = viewModel.loadChosen()

        dataAdapter.notifyDataSetChanged()
        resultAdapter.notifyDataSetChanged()
    }

    private inner class ResultAdapter(var dataSet: List<Spot>) : RecyclerView.Adapter<ResultAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.tourist_add_trip_result_item, parent, false))
        }

        override fun getItemCount(): Int = dataSet.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = dataSet[position].name

            if (position == itemCount - 1) {
                holder.clear.visibility = View.VISIBLE
                holder.clear.setOnClickListener { removeLast() }
            }
            else
                holder.clear.visibility = View.GONE

        }

        private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val name = itemView.findViewById<TextView>(R.id.t_add_trip_result_item_name)
            val clear = itemView.findViewById<ImageView>(R.id.t_add_trip_result_item_clear)
        }
    }

    private inner class DataAdapter(var dataSet: List<Spot>) : RecyclerView.Adapter<DataAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.tourist_add_trip_data_item, parent, false))
        }

        override fun getItemCount(): Int = dataSet.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = dataSet[position].name
            holder.code.text = dataSet[position].subGroup.code

            holder.name.setOnClickListener {
                chooseSpot(dataSet[position])
            }
        }

        private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val name = itemView.findViewById<TextView>(R.id.t_add_trip_data_item_name)
            val code = itemView.findViewById<TextView>(R.id.t_add_trip_data_item_code)
        }
    }

}
