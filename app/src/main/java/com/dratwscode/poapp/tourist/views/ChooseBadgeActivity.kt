package com.dratwscode.poapp.tourist.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.model.Badge
import com.dratwscode.poapp.tourist.model.Mockups
import kotlinx.android.synthetic.main.tourist_activity_choose_badge.*

class ChooseBadgeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tourist_activity_choose_badge)

        t_choose_badge_recycler.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(context)

            adapter = Adapter(Mockups.badgesData)
        }
    }

    fun onClick(position: Int) {
        val res = Intent()
        res.putExtra("res", position)
        setResult(42, res)
        finish()
    }


    private inner class Adapter(private val dataSet: List<Pair<Badge, Int>>) : RecyclerView.Adapter<Adapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val layout = LayoutInflater.from(parent.context).inflate(R.layout.tourist_choose_badge_item, parent, false)

            return ViewHolder(layout)
        }

        override fun getItemCount(): Int = dataSet.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = dataSet[position].first.name
            holder.imageView.setImageResource(dataSet[position].second)

            holder.name.setOnClickListener {
                onClick(position)
            }
        }

        private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val name = itemView.findViewById<TextView>(R.id.t_choose_bagde_item)!!
            val imageView = itemView.findViewById<ImageView>(R.id.t_choose_badge_item_image)!!
        }
    }
}
