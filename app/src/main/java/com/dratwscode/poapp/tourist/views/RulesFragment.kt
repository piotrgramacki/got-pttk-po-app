package com.dratwscode.poapp.tourist.views

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.viewmodels.RulesViewModel

class RulesFragment : Fragment() {

    companion object {
        fun newInstance() = RulesFragment()
    }

    private lateinit var viewModel: RulesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tourist_fragment_rules, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run { viewModel = ViewModelProviders.of(this).get(RulesViewModel::class.java) }
        // TODO: Use the ViewModel
    }

}
