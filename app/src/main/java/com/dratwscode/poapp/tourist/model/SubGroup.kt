package com.dratwscode.poapp.tourist.model

class SubGroup(val id : Int, val name: String, val code: String, val group: Group)