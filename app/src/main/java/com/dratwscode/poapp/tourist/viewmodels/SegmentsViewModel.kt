package com.dratwscode.poapp.tourist.viewmodels

import androidx.lifecycle.ViewModel
import com.dratwscode.poapp.tourist.model.Mockups
import com.dratwscode.poapp.tourist.model.Segment

class SegmentsViewModel : ViewModel() {

    fun getDataset(): List<Segment> {
        // TODO: change to use segmentsData form DB
        return Mockups.segmentsData.toList()
    }
}
