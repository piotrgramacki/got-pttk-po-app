package com.dratwscode.poapp.tourist.views


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.dratwscode.poapp.R
import com.dratwscode.poapp.admin.model.Badge
import com.dratwscode.poapp.tourist.model.Book
import com.dratwscode.poapp.tourist.model.Mockups
import com.dratwscode.poapp.tourist.viewmodels.BooksViewModel
import com.dratwscode.poapp.tourist.viewmodels.mappers.Mapper
import kotlinx.android.synthetic.main.tourist_fragment_add_book.*

class AddBookFragment : Fragment() {
    private lateinit var viewModel: BooksViewModel
    private var badgeId: Int = 0
    private var overflow: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tourist_fragment_add_book, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.run { viewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java) }

        val lastBadgeId = viewModel.getLastBadgeId()

        if (lastBadgeId != 0 && lastBadgeId < Mockups.NUMBER_OF_BADGES) {
            badgeId = lastBadgeId + 1
            t_add_book_badge_input.setText(Mockups.badgesData[badgeId].first.name)
            t_add_book_confirm.visibility = View.VISIBLE
        }

        val over = viewModel.getOverflow()

        if (over >= 0) {
            t_add_book_points_input.setText(over.toString())
            overflow = over
        }

        t_add_book_badge_input.setOnClickListener {
            val intent = Intent(context, ChooseBadgeActivity::class.java)
            startActivityForResult(intent, 12)
        }

        t_add_book_confirm.setOnClickListener {
            val pts_val = t_add_book_points_input.text.toString()

            if (pts_val != "")
                overflow = pts_val.toInt()
            else overflow = 0

            viewModel.saveBook(badgeId, overflow)

//            findNavController().popBackStack(R.id.summaryFragment, false)
            findNavController().navigateUp()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == 42) {
            val res = data?.getIntExtra("res", 0)

            if (res != null)
                badgeId = res

            t_add_book_badge_input.setText(Mockups.badgesData[badgeId].first.name)

            t_add_book_points_input.setText("")

            if (badgeId == 0)
                t_add_book_confirm.visibility = View.INVISIBLE
            else
                t_add_book_confirm.visibility = View.VISIBLE
        }
    }


}
