package com.dratwscode.poapp.tourist.model

class TripSegment(val id : Int, val trip : Trip?, val segment : Segment?,
                  val start : String?, val end : String?,
                  val height : Int?, val length : Int?) {
}