package com.dratwscode.poapp.tourist.viewmodels

import androidx.lifecycle.ViewModel
import com.dratwscode.poapp.communication.entities.TrasaTO
import com.dratwscode.poapp.communication.requesters.impl.tourist.BookTouristRequester
import com.dratwscode.poapp.communication.requesters.impl.tourist.TripSegmentTouristRequester
import com.dratwscode.poapp.communication.requesters.impl.tourist.SegmentsTouristRequester
import com.dratwscode.poapp.communication.requesters.impl.tourist.TripTouristRequester
import com.dratwscode.poapp.tourist.model.Segment
import com.dratwscode.poapp.tourist.model.Spot
import com.dratwscode.poapp.tourist.model.Trip
import com.dratwscode.poapp.tourist.viewmodels.mappers.Mapper
import java.sql.Date

class TripViewModel : ViewModel() {
    private val chosenSpots: MutableList<Spot> = mutableListOf()
    private val chosenSegments: MutableList<Segment> = mutableListOf()
    private val allSegments: MutableList<Segment> = mutableListOf()
    val requester = SegmentsTouristRequester.getInstance()
    val tripRequester = TripTouristRequester.getInstance()
    val tripSegmentRequster = TripSegmentTouristRequester.getInstance()
    val bookRequster = BookTouristRequester.getInstance()

    fun loadInitSegments(): List<Spot> {
        val segmentsList = requester.getAll
        val spotsMappedList = mutableListOf<Spot>()
        for (segment in segmentsList) {
            spotsMappedList.add(Mapper.spotFromTO(segment.punktpoczatkowy!!))
            allSegments.add(Mapper.segmentFromTO(segment))
        }
        return spotsMappedList
    }

    fun chooseSpot(spot: Spot): Int {
        return if (chosenSpots.isEmpty()) {
            chosenSpots.add(spot)
            0
        } else {
            val segment = allSegments.firstOrNull { it.begin?.id == chosenSpots.last().id && it.end?.id == spot.id }

            chosenSpots.add(spot)
            chosenSegments.add(segment!!)

            chosenSegments.fold(0) { acc, it -> acc + it.points }
        }
    }

    fun removeSpot(): Int {
        chosenSpots.removeAt(chosenSpots.size - 1)

        if (chosenSegments.isNotEmpty())
            chosenSegments.removeAt(chosenSegments.size - 1)

        return chosenSegments.fold(0) { acc, segment -> acc + segment.points }
    }

    fun getNextSpots(): List<Spot> {
        return if (chosenSpots.isEmpty()) {
            allSegments.map { it.begin } as List<Spot>
        } else {
            val lastSpot = chosenSpots.last()
            allSegments.filter { it.begin?.id == lastSpot.id }.map { it.end } as List<Spot>
        }
    }

    private fun loadSegmentsBySpot(spot: Spot): List<Segment> {
        val segments = requester.getBySpot(spot.id)
        return segments.map { Mapper.segmentFromTO(it) }
    }

    fun loadChosen(): List<Spot> {
        return chosenSpots
    }

    fun save(bookID: Int) {
        var points: Int = 0
        for (chosenSegment in chosenSegments) {
            points += chosenSegment.points
        }

        updateBook(bookID, points)

        val trasaTO = TrasaTO(0, null, Date(System.currentTimeMillis()), points)
        val newTripId = tripRequester.post(trasaTO, bookID)
        for (i in 0 until chosenSegments.size) {
            tripSegmentRequster.post(newTripId, chosenSegments[i].id, i)
        }
    }

    fun updateBook(bookID: Int, points: Int) {
        bookRequster.updateBook(bookID, points)
    }

    fun getTrips(bookId: Int): List<Trip> {
        val result = tripRequester.getAllById(bookId)
        return result.map { Mapper.tripFromTO(it) }
    }
}