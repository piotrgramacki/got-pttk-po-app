package com.dratwscode.poapp.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dratwscode.poapp.R
import com.dratwscode.poapp.tourist.views.MainActivity
import kotlinx.android.synthetic.main.login_activity_startpage.*

class StartPageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity_startpage)

        registerListeners()
    }

    private fun registerListeners() {
        startPage_tourist.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        startPage_admin.setOnClickListener {
            val intent = Intent(this, com.dratwscode.poapp.admin.views.MainActivity::class.java)
            startActivity(intent)
        }
    }
}
